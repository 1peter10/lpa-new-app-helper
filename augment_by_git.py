import subprocess
import os
import re
from datetime import datetime

def run_git_command(command, cwd=None):
    try:
        output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT, text=True, cwd=cwd)
        return output.strip()
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: '{command}' in {cwd}\nError: {e.output}")
        return None

def sanitize_directory_name(url):
    name = re.sub(r'[^a-zA-Z0-9]+', '_', url)
    return name[:50]

def extract_urls_from_markdown(file_path, excluded_domains=None):
    if excluded_domains is None:
        excluded_domains = []
    urls = []
    with open(file_path, 'r') as file:
        for line in file:
            found_urls = re.findall(r'\bhttps?://[^\s)\]>]+', line)
            for url in found_urls:
                domain = url.split('//')[-1].split('/')[0]
                if domain not in excluded_domains:
                    urls.append(url)
    return urls

def is_valid_repo_url(url):
    # Filter out URLs that are likely not cloneable repositories
    invalid_patterns = ['/pulls/', '/issues/', '/blob/', '/discussions/', 'community-wiki',]
    return not any(pattern in url for pattern in invalid_patterns)

def parse_datetime(date_str):
    try:
        return datetime.strptime(date_str, "%c")
    except ValueError:
        return None

def fetch_repo_data(repo_url, temp_dir):
    repo_name = sanitize_directory_name(repo_url)
    repo_path = os.path.join(temp_dir, repo_name)

    if os.path.exists(repo_path) and os.listdir(repo_path):
        subprocess.run(f"rm -rf {repo_path}", shell=True)

    clone_command = f"git clone --depth 1 {repo_url} {repo_path}"
    clone_result = run_git_command(clone_command)
    if clone_result is None or not os.path.exists(repo_path) or not os.listdir(repo_path):
        return "Clone failed", "Clone failed", None

    latest_tag_command = "git describe --tags `git rev-list --tags --max-count=1`"
    latest_tag = run_git_command(latest_tag_command, cwd=repo_path) or "No tag found"

    last_commit_date_command = "git log -1 --format=%cd"
    last_commit_date = run_git_command(last_commit_date_command, cwd=repo_path) or "No commit found"

    subprocess.run(f"rm -rf {repo_path}", shell=True)
    return latest_tag, last_commit_date, parse_datetime(last_commit_date)

def process_repositories(markdown_file, working_dir, excluded_domains, output_file):
    if not os.path.exists(working_dir):
        os.makedirs(working_dir)

    all_urls = extract_urls_from_markdown(markdown_file, excluded_domains)
    repo_urls = [url for url in all_urls if is_valid_repo_url(url)]
    results = []

    for repo_url in repo_urls:
        latest_tag, last_commit_date, last_commit_datetime = fetch_repo_data(repo_url, working_dir)
        results.append((repo_url, latest_tag, last_commit_date, last_commit_datetime))

    # Sort results by tag presence and last commit date
    results.sort(key=lambda x: (x[1] == "No tag found", x[3] is None, x[3]), reverse=True)

    write_results_to_file(results, output_file)

def write_results_to_file(results, output_file):
    with open(output_file, 'w') as file:
        for repo_url, latest_tag, last_commit_date, _ in results:
            file.write(f"* Repo: {repo_url}, Tag: {latest_tag}, Last Commit: {last_commit_date}\n")

# Configuration
markdown_file = 'apps-to-be-added.md'
working_dir = 'temp'  # Temporary working directory
excluded_domains = ['forums.puri.sm', 'flathub.org', 'youtube.com', 'www.youtube.com', 'himitsustore.org', 'web.archive.org', 'doc.qt.io', 'cubocore.org', 'wiki.postmarketos.org', 'wiki.gnome.org', 'stackoverflow.com', 'tchncs.de', 'puri.sm', 'carlschwan.eu', 'marius.bloggt-in-braunschweig.de',] 
output_file = 'augmented_data.md'  # Output file name

# Execute the process
process_repositories(markdown_file, working_dir, excluded_domains, output_file)
print(f"Results have been written to {output_file}")

