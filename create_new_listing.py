import argparse
import os
import re
import urllib.parse
from datetime import date

TEMPLATE_FRONTMATTER = '''\
title = "{name}"
description = ""
aliases = []
date = {date}

[taxonomies]
project_licenses = []
metadata_licenses = []
app_author = []
categories = []
mobile_compatibility = []
status = []
frameworks = []
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []

[extra]
repository = "{repository}"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "{app_id}"
scale_to_fit = ""
flathub = "{flathub}"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [{repology}]
appstream_xml_url = "{appstream_xml_url}"
reported_by = "{reported_by}"
updated_by = ""
'''

def create_new_app(name, repository='', app_id='', appstream_xml_url='', repology='', flathub=''):
    reported_by = input('Please enter your name: ')
    repology = ', '.join(f'"{i.strip()}"' for i in repology.split(',')) if repology else ''
    frontmatter = TEMPLATE_FRONTMATTER.format(
        name=name,
        repository=repository,
        app_id=app_id,
        appstream_xml_url=appstream_xml_url or "",
        date=date.today().strftime("%Y-%m-%d"),
        reported_by=reported_by,
        repology=repology, 
        flathub=flathub
    )

    if app_id == '':
        parsed_url = urllib.parse.urlparse(repository)
        path_parts = parsed_url.path.split('/')
        if len(path_parts) >= 3:
            author = path_parts[1]
            appname = path_parts[2]
            filename = f'noappid.{author.lower()}.{appname.lower()}'
        else:
            raise ValueError('Could not determine filename from repository URL')
    else:
        filename = app_id.lower()

    content = f'+++\n{frontmatter}\n+++\n\n'
    content += '### Description\n\n\n'
    content += '### Notice'

    with open(f'{filename}.md', 'w') as f:
        f.write(content)

    print(f'New app "{name}" created in file "{filename}.md" with App ID "{app_id}" and AppStream XML URL "{appstream_xml_url or ""}".')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create a new Markdown file with TOML-Frontmatter for a new app')
    parser.add_argument('name', help='Name of the app')
    parser.add_argument('repository', help='URL of the app"s source code repository')
    parser.add_argument('--app-id', help='App ID of the app', default='')
    parser.add_argument('--appstream-xml-url', help='URL of the app"s AppStream XML file', default=None)
    parser.add_argument('--repology', help='Repology of the app', default=None) 
    parser.add_argument('--flathub', help='Flathub listing of the app', default=None) 
    args = parser.parse_args()

    create_new_app(args.name, args.repository, app_id=args.app_id, appstream_xml_url=args.appstream_xml_url, repology=args.repology, flathub=args.flathub)

