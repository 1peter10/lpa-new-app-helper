#!/usr/bin/env python3
import feedparser
from datetime import datetime, timedelta

# URLs of the ATOM feeds
apps_feed_url = "https://linuxphoneapps.org/apps/atom.xml"
games_feed_url = "https://linuxphoneapps.org/games/atom.xml"

def fetch_and_parse_feed(url):
    # Fetch and parse the ATOM feed
    return feedparser.parse(url)

def filter_entries_by_date(entries, start_date, end_date):
    # Filter entries within the specified date range and sort them chronologically
    filtered_entries = [entry for entry in entries if start_date <= datetime(*entry.published_parsed[:6]) <= end_date]
    return sorted(filtered_entries, key=lambda x: x.published_parsed)

def format_entry(entry):
    title = entry.title
    url = entry.id
    summary = entry.summary
    thank_you_msg = ""
    if 'contributors' in entry and entry.contributors:  # Check if contributors exist
        for contributor in entry.contributors:
            # Safely access the 'name' key, continue to next contributor if 'name' is not found
            contributor_name = contributor.get('name')
            if contributor_name and contributor_name.lower() != '1peter10':  # Ignore linmob as a contributor for the thank you message
                author_name = entry.author if 'author' in entry else None
                if author_name and contributor_name == author_name:
                    thank_you_msg = f"Thank you, {contributor_name}, for creating and adding the app!"
                else:
                    thank_you_msg = f"Thank you, {contributor_name}, for adding the app!"
                break  # Assuming only one relevant contributor per entry

    return f"* [{title}]({url}) - {summary} {thank_you_msg}"

def generate_markdown_list(entries):
    # Generate markdown list with monthly subheadings if needed, in chronological order
    markdown_list = ""
    current_month = None
    for entry in entries:
        entry_date = datetime(*entry.published_parsed[:6])
        if len(entries) > 5:
            if current_month != entry_date.strftime("%B"):
                current_month = entry_date.strftime("%B")
                markdown_list += f"\n## {current_month}\n"
        markdown_list += format_entry(entry) + "\n"
    return markdown_list

def process_feed(url, start_date, end_date):
    feed = fetch_and_parse_feed(url)
    entries = filter_entries_by_date(feed.entries, start_date, end_date)
    return generate_markdown_list(entries)

# Determine the last completed quarter based on current date
today = datetime.today()
current_quarter = (today.month - 1) // 3 + 1
last_quarter = (current_quarter - 1) if current_quarter > 1 else 4
last_quarter_year = today.year if last_quarter != 4 else today.year - 1

# Calculate the last quarter's start and end dates
# Calculate the last quarter's start and end dates
quarter_start_month = (last_quarter - 1) * 3 + 1
quarter_end_month = last_quarter * 3
quarter_start_date = datetime(last_quarter_year, quarter_start_month, 1)

# Adjusted to ensure the end date includes the entire last month of the quarter
if quarter_end_month == 12:  # December, so set to the last day of the month
    quarter_end_date = datetime(last_quarter_year, quarter_end_month, 31)
else:
    # For other months, find the first day of the next month and subtract one day
    next_month = quarter_end_month + 1
    quarter_end_date = datetime(last_quarter_year, next_month, 1) - timedelta(days=1)

# Process each feed
apps_markdown = process_feed(apps_feed_url, quarter_start_date, quarter_end_date)
games_markdown = process_feed(games_feed_url, quarter_start_date, quarter_end_date)

print("# Apps\n")
print(apps_markdown)
print("# Games\n")
print(games_markdown)

