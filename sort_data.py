from datetime import datetime
from collections import defaultdict
from urllib.parse import urlparse

def parse_line(line):
    """Extract repo, tag, last commit date from a line, or detect failure, and extract project name."""
    parts = line.strip().split(", ")
    repo_url = parts[0].split(": ")[1]
    tag = parts[1].split(": ")[1]
    last_commit_str = parts[2].split(": ", 1)[1].strip()

    # Extract project name from URL
    path = urlparse(repo_url).path
    project_name = path.rstrip('/').split('/')[-1]

    if "Clone failed" in last_commit_str:
        return project_name, repo_url, tag, "Clone failed"
    
    commit_date = None
    if last_commit_str != "No commit found":
        try:
            commit_date = datetime.strptime(last_commit_str, '%a %b %d %H:%M:%S %Y %z')
        except ValueError:
            commit_date = "Clone failed"
    
    return project_name, repo_url, tag, commit_date

def sort_entries(entries):
    """Sort entries, separate clone failures, and order by year descending."""
    with_tag = []
    without_tag = []
    failures = []
    
    for entry in entries:
        project_name, repo, tag, commit_date = parse_line(entry)
        if commit_date == "Clone failed":
            failures.append((project_name, repo, tag))
        elif tag != "No tag found" and commit_date:
            with_tag.append((project_name, repo, tag, commit_date))
        elif commit_date:
            without_tag.append((project_name, repo, tag, commit_date))
    
    with_tag.sort(key=lambda x: (x[3].year, x[3]), reverse=True)
    without_tag.sort(key=lambda x: (x[3].year if x[3] else 0, x[3] if x[3] else datetime.min), reverse=True)
    
    return with_tag + without_tag, failures

def create_markdown(sorted_entries, failures):
    """Create markdown content from sorted entries and failures, newest first, including project names."""
    markdown_content = ""
    current_year = None
    
    for project_name, repo, tag, commit_date in sorted_entries:
        year = commit_date.year if commit_date else "No release date found"
        if year != current_year:
            markdown_content += f"### {year}\n" if year != "No release date found" else "### No release date found\n"
            current_year = year
        
        commit_date_str = commit_date.strftime('%a %b %d %H:%M:%S %Y %z') if commit_date else "No commit found"
        markdown_content += f"* {project_name} - Repo: {repo}, Tag: {tag}, Last Commit: {commit_date_str}\n"

    if failures:
        markdown_content += "### Failures\n"
        for project_name, repo, tag in failures:
            markdown_content += f"* {project_name} - Repo: {repo}, Tag: {tag}, Last Commit: Clone failed\n"
    
    return markdown_content

def process_file(input_file, output_file):
    """Read the input file, sort the entries from newest to oldest, separate failures, and write to the output file."""
    with open(input_file, 'r') as f:
        entries = f.readlines()
    
    sorted_entries, failures = sort_entries(entries)
    markdown_content = create_markdown(sorted_entries, failures)
    
    with open(output_file, 'w') as f:
        f.write(markdown_content)

# Adjust these file paths as necessary
input_file = 'augmented_data.md'
output_file = 'sorted_data.md'

process_file(input_file, output_file)

