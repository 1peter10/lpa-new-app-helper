#!/usr/bin/env bash

# Directory paths (adjust these to your actual directory names)
dir1="linuxphoneapps.frama.io/content/apps"
dir2="linuxphoneapps.frama.io/content/games"

# Function to search files
search_files() {
    grep -rl "appstream_xml_url = \"\"" $1 | while read -r file; do
        if ! grep -q "No Appstream Metadata (manual maintenance)" "$file"; then
            echo "$file"
            python add_tag.py "$file"
        fi
    done
}

# Run search on both directories
search_files $dir1
search_files $dir2

