#!/usr/bin/env bash

# Directory paths (adjust these to your actual directory names)
dir1="linuxphoneapps.frama.io/content/apps"
dir2="linuxphoneapps.frama.io/content/games"

# Function to search files
search_files() {
    echo "Searching in directory: $1"
    # Find files matching a simpler pattern
    find "$1" -type f -exec grep -l "packaged_in = \[\]" {} \; | while read -r file; do
        echo "Checking file: $file"
        # Check for other conditions one at a time
        if grep -q "flathub = \"\"" "$file" && ! grep -q "not packaged yet" "$file"; then
            echo "Match found: $file"
	    python add_tag_packaged.py "$file"
        fi
    done
}

# Run search on both directories
search_files $dir1
search_files $dir2

