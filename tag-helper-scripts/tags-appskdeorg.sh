#!/usr/bin/env bash

# Directory paths (adjust these to your actual directory names)
dir1="linuxphoneapps.frama.io/content/apps"
dir2="linuxphoneapps.frama.io/content/games"

# Function to search files
search_files() {
    # Find files containing 'appstream_xml_url = ""' but not containing the unwanted phrase
    grep -rl "https:\/\/apps.kde.org\/" $1 | while read -r file; do
        if ! grep -q "apps.kde.org\"" "$file"; then
            echo "$file"
            python add_tag_appskdeorg.py "$file"
	fi
    done
}

# Run search on both directories
search_files $dir1
search_files $dir2

