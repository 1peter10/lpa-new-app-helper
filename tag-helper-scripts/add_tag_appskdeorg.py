import toml
import sys

def add_tag(file_path, new_tag):
    with open(file_path, 'r+') as file:
        content = file.read()
        # Split content into TOML frontmatter and the rest
        parts = content.split('+++')
        if len(parts) < 3:
            return  # No valid TOML frontmatter found

        # Parse TOML frontmatter
        toml_content = toml.loads(parts[1])
        
        # Add new tag if it's not already there
        if 'taxonomies' not in toml_content:
            toml_content['taxonomies'] = {'tags': []}
        if 'tags' not in toml_content['taxonomies']:
            toml_content['taxonomies']['tags'] = []

        if new_tag not in toml_content['taxonomies']['tags']:
            toml_content['taxonomies']['tags'].append(new_tag)
        
        # Recreate TOML frontmatter
        updated_toml = toml.dumps(toml_content)
        
        # Write changes back to file
        file.seek(0)
        file.write('+++\n' + updated_toml + '+++\n' + parts[2])
        file.truncate()

if __name__ == "__main__":
    add_tag(sys.argv[1], "apps.kde.org")


