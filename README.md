
# Personal Helper scripts for LinuxPhoneApps.org

This/these things may/should eventually move into the LinuxPhoneApps project.

## Create New Listing (create_new_listing.py)

This script creates a new Markdown file with TOML-Frontmatter for a new app listing for [LinuxPhoneApps.org](https://linuxphoneapps.org).

### Requirements

- Python 3.x

### Usage

To create a new app, run the script with the following command:

```
python create_new_app.py <name> <repository> [--app-id APP_ID] [--appstream-xml-url APPSTREAM_XML_URL]
```

- `<name>`: The name of the app (required).
- `<repository>`: The URL of the app's source code repository (required).
- `--app-id APP_ID`: The App ID of the app (optional, recommended). If not specified, the filename will be generated from the repository URL.
- `--appstream-xml-url APPSTREAM_XML_URL`: The URL of the app's AppStream XML file (optional, recommended).
- `--repology REPOLOGY`: Package name on repology (optional, fill if exists)
- `--flathub FLATHUB_URL`: URL of the app on flathub.org (optional, fill if exists, make sure to remove language parameters in URL (e.g., /de/ when your locale is non-english))

If you run the script without any parameters, you will be prompted to enter the required information:

```
python create_new_app.py
```

The script will ask for your name, so that you can be properly credited for your contribution.

After you enter the required information, the script will create a new Markdown file with the following filename format:

- `<app_id>.md` if `app_id` is specified.
- `noappid.<author>.<appname>.md` if `app_id` is not specified and the repository URL is `https://codeforge.com/<author>/<appname>/`.

The file will be created in the current directory with the following TOML-Frontmatter:

```
title = "<name>"
description = ""
aliases = []
date = <current date formatted YYYY-MM-DD>

[taxonomies]
project_licenses = []
metadata_licenses = []
app_author = []
categories = []
mobile_compatibility = []
status = []
frameworks = []
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
[extra]
repository = "<repository>"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "<app_id>"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "<appstream_xml_url>"
reported_by = "<user inputted name>"
updated_by = ""
```

###  Have the checkers assist you with getting the listing ready

After coming up with a basic listing thanks to this script, you can then use the [checkers from the LinuxPhoneApps repo](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/tree/main/checkers) to futher augment the listing.

